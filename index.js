"use strict";

require('dotenv').config({silent: true});

let stations = require('./lib/stations').getAll();
let webServer = require('./lib/web/server');
let dbWrapper = require('./lib/database/jugglingdb');

dbWrapper(process.env.DATABASE_URL, (db) => {
    function fetchSongs() {
        console.log("Fetching songs... " + (new Date).toISOString());

        for(let station of entries(stations)) {
            station.getCurrent(s => db.save(station.name, s.artist, s.song, s.raw));
        }
    }

    fetchSongs();
    setInterval(fetchSongs, process.env.FETCH_INTERVAL_MILLIS);

    webServer.run(process.env.PORT, db);
});

function* entries(obj) {
    for(let key of Object.keys(obj)) {
        yield obj[key];
    }
}
