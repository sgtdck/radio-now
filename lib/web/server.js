"use strict";

let http = require('http');
let express = require('express');
let exphbs = require('express-handlebars');

let app = express();

let hbs = exphbs.create({
    defaultLayout: 'main',
    extname: '.hbs'
});

app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');

hbs.handlebars.registerHelper("percentage", function(lvalue, rvalue) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);

    return Math.round((lvalue / rvalue) * 100);
});

module.exports = {
    run: (port, db) => {
        app.get('/', (req, res) => {
            db.lastSongs((err, tuples) => {
                if(err) {
                    res.end("ERROR");
                    return;
                }

                res.render('home', {lastSongs: tuples});
            });
        });

        app.get('/stats', (req, res) => {
            db.allVsUniqueSongs((err, tuples) => {
                let totals = {
                    all: 0,
                    unique: 0
                };
                for(let i in tuples) {
                    totals.all += +tuples[i].all;
                    totals.unique += +tuples[i].unique;
                }
                res.render('stats', {stats: tuples, totals: totals});
            });
        });


        app.listen(port, () => {
            console.log("Server listening on port %s", port);
        });
    }
};
