"use strict";

let request = require('request');

function mapObject(obj) {
    return {
        'artist': obj.now_playing.artist_title,
        'song': obj.now_playing.song_title,
        'raw': JSON.stringify(obj)
    };
}

module.exports = {
    name: "SmoothFM 95.3",
    getCurrent:  (callback) => {
        request({
            url: "http://www.smooth.com.au/smooth/ajax/media_bar/smooth953?ajax_ts=1",
            json: true
        }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                callback(mapObject(body));
            }
        })
    }
};