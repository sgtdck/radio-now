"use strict";

let request = require('request');

function mapObject(obj) {
    return {
        'artist': obj.artist_title,
        'song': obj.song_title,
        'raw': JSON.stringify(obj)
    };
}

module.exports = {
    name: "Nova 96.9",
    getCurrent:  (callback) => {
        request({
            url: "http://prod-filesbucket-7hmmorphht20.s3-ap-southeast-2.amazonaws.com/nova-player-history/nova969-current.json",
            json: true
        }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                callback(mapObject(body));
            }
        })
    }
};