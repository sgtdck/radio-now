"use strict";

let request = require('request');

function mapObject(obj) {
    let subj = getNowOrPrev(obj);

    if(subj && subj.recording && subj.recording.artists) {
        return {
            'artist': subj.recording.artists.map(t => t.name).join(" ft. "),
            'song': subj.recording.title,
            'raw': JSON.stringify(subj)
        };
    }
}

function getNowOrPrev(obj) {
    if(obj.now && !Array.isArray(obj.now)) {
        return obj.now;
    } else if(obj.prev && !Array.isArray(obj.prev)) {
        return obj.prev;
    }
}

module.exports = {
    name: "Triple J",
    getCurrent:  (callback) => {
        request({
            url: "https://music.abcradio.net.au/api/v1/plays/triplej/now.json",
            json: true
        }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                callback(mapObject(body));
            }
        })
    }
};