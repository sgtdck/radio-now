"use strict";

let request = require('request');
let ls = require("../stream/lightstreamer");
let http = require('http');

function mapObject(obj) {
    if(obj && typeof obj === 'object') {
        for(var i in obj) {
            if(obj.hasOwnProperty(i) && obj[i] instanceof Array) {
                var data = obj[i];

                try {
                    var playing = data
                        .filter(t => typeof t === "string")
                        .map(t => JSON.parse(t))
                        .filter(t => t.Status === 'playing' && t.Artist !== "" && t.Title !== "");

                    if(playing.length === 1) {
                        return {
                            'artist': playing[0].Artist,
                            'song': playing[0].Title,
                            'raw': JSON.stringify(obj)
                        };
                    } else {
                        return;
                    }
                } catch (e) {
                    console.log("Unexpected error", e);
                    return;
                }
            }
        }
    }
}

function startLightstream(dataCallback, statusCallback) {
    var simpleLoggerProvider = new ls.SimpleLoggerProvider;

    var fc = new ls.FunctionAppender("FATAL","*",function(n) {
        console.log("[FATAL] LightStreamer: " + n)
    });

    simpleLoggerProvider.addLoggerAppender(fc);

    var serverAddress = "http://ls.triplem.com.au:80";
    var setAdapter = "NowPlaying";
    var lightStreamer = new ls.LightstreamerClient(serverAddress, setAdapter);

    ls.LightstreamerClient.setLoggerProvider(simpleLoggerProvider);

    lightStreamer.addListener({
        onStatusChange: function(newStatus) {
            statusCallback(newStatus);
        }
    });
    lightStreamer.connect();

    var subscription = new ls.Subscription("MERGE",["2MMM_FM"],["playing", "history1", "history2", "history3", "history4", "history5", "history6", "history7", "history8", "history9", "history"]);
    subscription.setDataAdapter("MONITOR");
    subscription.setRequestedSnapshot("yes");
    subscription.addListener({
        onItemUpdate: dataCallback
    });
    lightStreamer.subscribe(subscription);
}

let station = {
    started: false,
    name: "Triple M",
    getCurrent: (callback) => {
        if(!station.started) {
            station.started = true;

            startLightstream((data) => {
                callback(mapObject(data));
            }, (status) => {
                console.log(status + " to " + station.name + " streaming server");
            });
        }
    }
};

module.exports = station;
