"use strict";

let request = require('request');

function mapObject(obj) {
    return {
        'artist': obj.NowPlaying.Artist,
        'song': obj.NowPlaying.Title,
        'raw': JSON.stringify(obj)
    };
}

module.exports = {
    name: "WSFM 101.7",
    getCurrent:  (callback) => {
        request({
            url: "http://www.wsfm.com.au/umbraco/Arn/ArnFeed/GetOnAir?feedUrl=http://media.arn.com.au/xml/wsfm1017_now.xml",
            json: true
        }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                if(body && body.NowPlaying && body.NowPlaying.Type === "song") {
                    callback(mapObject(body));
                }
            }
        })
    }
};
