"use strict";

let request = require('request');

function mapObject(obj) {
    return {
        'artist': obj.NowPlaying.Artist,
        'song': obj.NowPlaying.Title,
        'raw': JSON.stringify(obj)
    };
}

module.exports = {
    name: "KIIS 1065",
    getCurrent:  (callback) => {
        request({
            url: "http://www.kiis1065.com.au/umbraco/Arn/ArnFeed/GetOnAir?feedUrl=http://media.arn.com.au/xml/mix1065_now.xml",
            json: true
        }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                if(body && body.NowPlaying && body.NowPlaying.Type === "song") {
                    callback(mapObject(body));
                }
            }
        })
    }
};
