"use strict";

let Schema = require('jugglingdb').Schema;

module.exports = (url, callback) => {
    let schema = new Schema('postgres', {url: url});

    let Station = schema.define('Station', {
        name: String
    }, {
        indexes: {
            station_name_uniq: {
                columns: "name",
                type: "btree",
                kind: "unique"
            }
        }
    });

    let Song = schema.define('Song', {
        date:       {type: Date, "default": () => new Date, index: true},
        title:      {type: String, index: true},
        artist:     {type: String, index: true},
        raw:        String
    }, {
        indexes: {
            song_stationId_idx: {
                columns: "\"stationId\"",
                type: "btree"
            }
        }
    });

    Station.hasMany(Song, {as: 'songs', foreignKey: 'stationId'});
    Song.belongsTo(Station, {as: 'station', foreignKey: 'stationId'});

    let save = (stationName, artistName, songTitle, rawData) => {
        let data = {name: stationName};
        let query = {where: data};

        Station.findOrCreate(query, data, (err, station) => {
            if(err) {
                console.log("Error occurred finding/creating a station", err);
            }

            Song.findOne({where: {stationId: station.id}, order: 'date DESC'}, (err, prevSong) => {
                if(err) {
                    console.log("Error occurred finding a song", err);
                }

                if(prevSong && prevSong.title === songTitle && prevSong.artist === artistName) {}
                else {
                    let song = station.songs.build({title: songTitle, artist: artistName, raw: rawData});
                    song.save();
                }
            });
        });
    };

    /**
     * Returns the last songs for each station
     * @param callback (err, data{date,artist,title,station})
     */
    let lastSongs = (callback) => {
        schema.adapter.query(`SELECT * FROM (SELECT DISTINCT on(S."stationId") S.date, S.artist, S.title, St.name AS "station"
FROM "public"."Song" as S
LEFT JOIN "public"."Station" as St ON S."stationId"=St.id
ORDER BY S."stationId" ASC, S.date DESC) p
ORDER BY p.date DESC`, callback);
    };

    /**
     * Returns total # of songs + total # of unique songs per station
     * @param callback (err, data{station, all, unique})
     */
    let allVsUniqueSongs = (callback) => {
        schema.adapter.query(`SELECT DISTINCT ON(s."stationId")
	st.name AS station,
	COUNT(*) "all",
	(SELECT COUNT(*) FROM (SELECT DISTINCT ON(s1.artist, s1.title) COUNT(*) c
	    FROM public."Song" s1
	    WHERE s1."stationId"=s."stationId"
	    GROUP BY s1.artist, s1.title) p) "unique"
FROM public."Song" s
LEFT JOIN public."Station" st ON st.id=s."stationId"
GROUP BY s."stationId", st.name`, callback);
    };

    let orm = {
        save: save,
        lastSongs: lastSongs,
        allVsUniqueSongs: allVsUniqueSongs
    };

    schema.isActual((err, actual) => {
        if(!actual) {
            console.log("Updating schema...");
            schema.autoupdate(() => {
                console.log("Done updating schema.");
                callback(orm);
            });
        } else {
            callback(orm);
        }
    });
};
