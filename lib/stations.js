"use strict";

module.exports = (() => {
    let list = {};

    function init() {
        requireAll();
    }

    function requireAll() {
        let instances = {};

        var normalizedPath = require("path").join(__dirname, "stations");

        require("fs").readdirSync(normalizedPath).forEach(file => {
            let station = require('./stations/' + file);
            if(station) {
                instances[file] = station;
                list[file] = instances[file];
            }
        });

        return instances;
    }

    this.exists = (id) => list.hasOwnProperty;

    this.get = (id) => {
        if(this.exists(id)) {
            return list[id];
        }
    };

    this.getAll = () => list;

    init();
    return this;
})();
